import argparse
import json
from lxml import etree

parser = argparse.ArgumentParser()
parser.add_argument('filename')
parser.add_argument('output')
args = parser.parse_args()
fn = args.filename
out = args.output

opml = etree.parse(fn)
urls = opml.xpath('//outline[boolean(@type)]/@htmlUrl')
titles = opml.xpath('//outline[boolean(@type) and @htmlUrl]/@title')
out_dict = {}
for i in enumerate(urls):
  print i
  out_dict[titles[i[0]]] = i[1]

with open(out, 'a') as outfile:
  outfile.write(json.dumps(out_dict))
