# -*- coding: utf-8 -*-
from scrapy import Spider, Request
from lxml import html, etree
from articles.items import ArticlesItem
import json
import re


class GenericSpider(Spider):
    name = "generic.article"

    def __init__(self, file_path=None, *args, **kwargs):
      super(Spider, self).__init__(*args, **kwargs)

      if file_path:
        with open(file_path, 'r') as infile:
          data = json.load(infile)
        self.start_urls = [data[x] for x in data.keys()]
      else:
        self.start_urls = []

      self.url_patterns = {r'blogspot\.com': self.parse_blogspot}
      self.body_patterns = {}

    def start_requests(self):
      for url in self.start_urls:

        # Try to identify website from URL
        found = False
        for pattern in self.url_patterns.keys():
          if re.search(pattern, url):
            found = True
            yield Request(url=url, callback=self.url_patterns[pattern])
            break

        if not found:
          yield Request(url=url, callback=self.preprocess_page)

    def preprocess_page(self, response):
      # Tries to identify a website from its homepage and calls the appropriate crawling method
      # If website is unknown, uses generic crawling method
      page_body = response.body_as_unicode()
      for pattern in self.url_patterns.keys():
        if re.search(pattern, page_body):
          yield Request(url=response.url, callback=self.body_patterns[pattern])

    # ================
    # SPECIFIC CRAWLS
    # ================

    def parse_blogspot(self, response):
      page_body = response.body_as_unicode()
      xml = html.fromstring(page_body)

      next_page_link_list = xml.xpath('//a[@class="blog-pager-older-link"]/@href')

      # Extract posts from current page
      articles = xml.xpath('//div[@itemprop="blogPost"]')

      for article in articles:
        raw_body = article.xpath('.//div[@itemprop="description articleBody"]')[0]
        article_body = '\n'.join(filter(lambda x: bool(x), map(str.strip, map(lambda y: str(y.encode('utf-8')), raw_body.xpath('.//div/text()|.//span/text()|text()'))))).strip()
        links = filter(lambda x: bool(x), map(str.strip, article.xpath('.//a/@href')))
        post_link_raw = article.xpath('.//h3[@itemprop="name"]/a/@href')

        if post_link_raw:
          post_link = post_link_raw[0]
        else:
          post_link = None

        if article_body or links:
          item = ArticlesItem()
          item['url'] = post_link
          # myItem.html = etree.tostring(article)
          item['links'] = links
          item['text'] = article_body
          yield item


      if next_page_link_list and next_page_link_list[0]:
        yield Request(url=next_page_link_list[0], callback=self.parse_blogspot)
