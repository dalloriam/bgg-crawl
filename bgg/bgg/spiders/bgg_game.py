# -*- coding: utf-8 -*-
from bgg.items import Boardgame
from scrapy import Request, Spider
from lxml import html


class BggGameSpider(Spider):
  name = "bgg.game"
  allowed_domains = ['boardgamegeek.com']
  start_urls = (
      'http://boardgamegeek.com/browse/boardgame',
  )

  base_url = 'http://boardgamegeek.com'

  def parse(self, response):
    body = response.body_as_unicode()
    xml = html.fromstring(body)
    ln_nextpage = xml.xpath('//a[@title="next page"]/@href')

    id_list = xml.xpath('//tr[@id="row_"]/td[@class="collection_thumbnail"]/a/@href')

    for game_id in id_list:
      yield Request(url=self.base_url+game_id, callback=self.parse_game)

    if len(ln_nextpage) != 0 and ln_nextpage[0]:
      yield Request(url=self.base_url + '/' + ln_nextpage[0], callback=self.parse)

  def parse_game(self, response):
    body = response.body
    xml = html.fromstring(body)
    game = Boardgame()
    game['game_id'] = response.url.split('/')[-2]
    game['title'] = xml.xpath('//meta[@name="title"]/@content')[0]
    yield game
